import unittest
from datetime import datetime
from zoneinfo import ZoneInfo

from src.py_jitutils.jitutils import JitTimes


class MyTestCase_JitTimes(unittest.TestCase):
    def setUp( self,
               TS: float = '',  # datetime.now().timestamp(),
               dt_format: str = '%Y-%m-%d %H:%M:%S',
               tz_info: str = '' ) -> None:
        self.time_stamp = 1517867109.1652
        self.dt_now = datetime.timestamp(datetime.now())
        self.TS = TS
        self.dt_format = dt_format
        self.tz_info = tz_info

    '''
    ทดสอบ Error Message โดยที่ไม่ใส่ Time Stamp
    '''

    def test_fail_not_TimeStamp( self ):
        # TODO Arrange 'สำหรับกำหนดค่าเริ่มต้นของการทดสอบ'
        jt = JitTimes(TS=self.TS)

        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertEqual('an integer is required (got type str)', jt.__str__(),
                         msg='Test error `TypeError`')  # Error Except `TypeError`

    '''
    ทดสอบการใส่ค่า Time Stamp
    '''

    def test_success_arg_time_stamp( self ):
        # TODO Arrange 'สำหรับกำหนดค่าเริ่มต้นของการทดสอบ'
        self.TS = 1517867109.1652

        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jt = JitTimes(TS=self.TS)
        # print(jt.__str__())

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertEqual(first='2018-02-06 04:45:09', second=jt.__str__(), msg='ทดสอบ datetime ที่ถูกต้อง')
        self.assertNotEqual('2019-02-06 04:45:09', jt.__str__(), msg='ทดสอบค่าที่ไม่ตรง')

    '''
    ทดสอบการแสดงผลโดยกำหนด format date time ในการแสดงผล
    '''

    def test_dateTime_return_format( self ):
        # TODO Arrange 'สำหรับกำหนดค่าเริ่มต้นของการทดสอบ'
        ts = self.time_stamp

        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jt = JitTimes(TS=ts, dt_format=self.dt_format)

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertEqual(first='2018-02-06 04:45:09', second=jt.__str__(), msg='ทดสอบ datetime ที่ถูกต้อง')
        self.assertNotEqual('2019-02-06', jt.__str__(), msg='ทดสอบค่าที่ไม่ตรง')

        # TODO Act and Assert
        jt = JitTimes(TS=ts, dt_format="%Y")  # ตั้งค่า datetime_format แสดงแค่ `ปี`
        self.assertEqual(first=str(2018), second=jt.__str__(), msg='ทดสอบแสดงค่าแค่ ปี')
        self.assertNotEqual(first='2018-02-06 04:45:09', second=jt.__str__(), msg='ทดสอบแสดงค่าแค่ ปี (fail)')
        self.assertNotEqual(first=str(2019), second=jt.__str__(), msg='ทดสอบแสดงค่าแค่ ปี (fail)')

        jt = JitTimes(TS=ts, dt_format="%y")  # ตั้งค่า datetime_format แสดงแค่ `ปี` 2 หลัก
        self.assertEqual(first=str(18), second=jt.__str__(), msg='ทดสอบแสดงค่าแค่ ปี 2 หลัก')
        self.assertNotEqual(first=str(2019), second=jt.__str__(), msg='ทดสอบแสดงค่าแค่ ปี 2 หลัก (fail)')

        jt = JitTimes(TS=ts, dt_format="%I:%M %p")  # ตั้งค่า datetime_format แสดงแค่ `24 hr PM/AM`
        self.assertEqual(first='04:45 AM', second=jt.__str__(), msg='ทดสอบแสดงค่าแค่ 24 hr PM/AM (success)')
        self.assertNotEqual(first='04:45:09', second=jt.__str__(), msg='ทดสอบแสดงค่าแค่ 24 hr PM/AM  (fail)')

    '''
    ทดสแบ Time Zone
    '''

    def test_TimeZone( self ) -> None:
        # TODO Arrange 'สำหรับกำหนดค่าเริ่มต้นของการทดสอบ'
        ts = self.time_stamp
        tz_name_empty_fail = ''
        tz_name_fail = 'Los_Angeles'
        self.tz_info = str(ZoneInfo('America/Los_Angeles'))

        # TODO Act and Assert
        jt = JitTimes(TS=ts, tz_info=self.tz_info)
        self.assertEqual(first='2018-02-05 13:45:09', second=jt.__str__(), msg='ทดสอบ Zone info (success)')
        self.assertNotEqual(first='2018-02-06 04:45:09', second=jt.__str__(), msg='ทดสอบ Zone info (fail)')

        '''
        ทดสอบ Fails Error return message
        '''
        jt = JitTimes(TS=ts, tz_info=tz_name_empty_fail)
        self.assertEqual(first='2018-02-06 04:45:09', second=jt.__str__(), msg='ทดสอบ Zone info เป็นค่าว่าง (success)')
        self.assertNotEqual(first='2018-02-05 13:45:09', second=jt.__str__(), msg='ทดสอบ Zone info เป็นค่าว่าง (fail)')

        jt = JitTimes(TS=ts, tz_info=tz_name_fail)
        self.assertEqual(first="'str' object has no attribute 'strftime'", second=jt.__str__(),
                         msg="AttributeError: 'str' object has no attribute 'strftime'")

        jt = JitTimes(TS=ts, tz_info=None)
        self.assertEqual(first="'dict' object has no attribute 'strftime'", second=jt.__str__(),
                         msg="RecursionError: maximum recursion depth exceeded while calling a Python object")
        self.assertNotEqual(first='', second=jt.__str__(), msg='ต้องมีการแสดงค่า Error ออกมา')

    '''
    ทดสอบ แปลง ค.ศ. เป็น พ.ศ. และ ฮ.ศ.
    '''

    def test_Convert_Era( self ) -> None:
        # TODO Arrange 'สำหรับกำหนดค่าเริ่มต้นของการทดสอบ'
        ts = self.time_stamp
        self.tz_info = str(ZoneInfo('America/Los_Angeles'))

        # TODO Act 'สำหรับ execute ส่วนที่ต้องการทดสอบหรือ business logic'
        jtBuddhistEraTz = JitTimes(TS=ts, tz_info=self.tz_info)
        buddhistEraTz = jtBuddhistEraTz.buddhistEra()

        jtBuddhistEraDtTz = JitTimes(TS=ts, dt_format="%x", tz_info=self.tz_info)
        buddhistEraDtTz = jtBuddhistEraDtTz.buddhistEra()

        jtHijriEraTZ = JitTimes(TS=ts, tz_info=self.tz_info)
        hijriEraTz = jtHijriEraTZ.hijriEra()

        jtHijriEraDtTZ = JitTimes(TS=ts, dt_format="%x", tz_info=self.tz_info)
        hijriEraDtTZ = jtHijriEraDtTZ.buddhistEra()

        # TODO Assert 'สำหรับการตรวจสอบว่า ผลการทำงานตรงตามที่คาดหวังหรือไม่ ต้องมีทุก test นะขาดไม่ได้'
        self.assertEqual(first='2561-02-05 13:45:09', second=buddhistEraTz, msg='ทดสอบ พ.ศ. แบบใส่ Time zone')
        self.assertIsNotNone(obj=buddhistEraTz, msg='ทดสอบค่าว่าง')

        self.assertEqual(first='02/05/61', second=buddhistEraDtTz,
                         msg='ทดสอบ พ.ศ. แบบกำหนด date time format และ Timezone')
        self.assertNotEqual(first='61/05/02', second=buddhistEraDtTz,
                            msg='ทดสอบ พ.ศ. แบบกำหนด date time format และ Timezone')
        self.assertIsNotNone(obj=buddhistEraDtTz, msg='ทดสอบค่าว่าง พ.ศ. แบบกำหนด date time format และ Timezone')

        self.assertEqual(first='1397-02-05 14:10:07', second=hijriEraTz, msg='ทดสอบ ฮ.ศ. แบบใส่ Time zone')
        self.assertNotEqual(first='1397-02-05', second=hijriEraTz, msg='ทดสอบค่าว่าง ฮ.ศ. แบบใส่ Time zone')
        self.assertIsNotNone(obj=hijriEraTz, msg='ทดสอบ ฮ.ศ. แบบใส่ Time zone')

        self.assertEqual(first='02/05/61', second=hijriEraDtTZ, msg='ทดสอบ ฮ.ศ. แบบกำหนด date time format และ Timezone')
        self.assertIsNotNone(obj=hijriEraDtTZ, msg='ทดสอบ ฮ.ศ. แบบกำหนด date time format และ Timezone (ค่าว่าง)')

        # TODO Act and Assert
        print('0--------')
        pass

    def tearDown( self ) -> None:
        self.time_stamp = None


if __name__ == '__main__':
    unittest.main()
